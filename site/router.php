<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * @param	array	A named array
 * @return	array
 */
function DzstreamBuildRoute(&$query) {
    $segments = array();

    // get a menu item based on Itemid or currently active
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $params = JComponentHelper::getParams('com_dzstream');
    $advanced = $params->get('sef_advanced_link', 0);

    // we need a menu item.  Either the one specified in the query, or the current active one if none specified
    if (empty($query['Itemid'])) {
        $menuItem = $menu->getActive();
    } else {
        $menuItem = $menu->getItem($query['Itemid']);
    }
    
    $mView = (empty($menuItem->query['view'])) ? null : $menuItem->query['view'];
    $mId = (empty($menuItem->query['id'])) ? null : $menuItem->query['id'];
    
    if (isset($query['view'])) {
        if (!(($mView == 'channels' && $query['view'] == 'channel') || $query['view'] == $mView)) {
            $segments[] = $query['view'];
        }
        unset($query['view']);
    }
    if (isset($query['id'])) {
        if ($mId != (int) $query['id']) {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT alias FROM #__dzstream_channels WHERE id = " . (int) $query['id']);
            $alias = $db->loadResult();
            $segments[] = ($alias) ? $alias : $query['id'];
        }
        unset($query['id']);
    }

    return $segments;
}

/**
 * @param	array	A named array
 * @param	array
 *
 * Formats:
 *
 * index.php?/dzstream/task/id/Itemid
 *
 * index.php?/dzstream/id/Itemid
 */
function DzstreamParseRoute($segments) {
    $vars = array();
    
    //Get the active menu item.
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $item = $menu->getActive();

    // Count segments
    $count = count($segments);
    if ($count == 1 && $item && $segments[0]) {
        // This mean we only have the id, view is implied from the menu
        if ($item->query['view'] == 'channels') {
            $vars['view'] = 'channel';
        }
    } else {
        // View is always first in other cases
        $vars['view'] = array_shift($segments);
    }

    // Id is always last segment
    if (count($segments)) {
        $vars['id'] = array_pop($segments);
        if (!(int) $vars['id']) {
            $db = JFactory::getDbo();
            $db->setQuery('SELECT id FROM #__dzstream_channels WHERE alias = ' . $db->quote(str_replace(':', '-', $vars['id'])));
            $id = $db->loadResult();
            $vars['id'] = $id;
        }
    }
    
    return $vars;
}
