<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_dzstream
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * DZStream Component Category Tree
 *
 * @static
 * @package     Joomla.Site
 * @subpackage  com_dzvideo
 * @since       3.0
 */
class DZStreamCategories extends JCategories
{
    public function __construct($options = array())
    {
        $options['table'] = '#__dzstream_channels';
        $options['extension'] = 'com_dzstream';
        parent::__construct($options);
    }
}
