<?php
/**
* @version     1.0.0
* @package     com_dzstream
* @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
* @license     bản quyền mã nguồn mở GNU phiên bản 2
* @author      DZ Team <support@dzdev.com> - dzdev.com
*/
// no direct access
defined('_JEXEC') or die;


?>
<div class="item_fields component-inner">
<?php if ($this->item) : ?>
	<table class="table">
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_ID'); ?></th>
			<td><?php echo $this->item->id; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_NAME'); ?></th>
			<td><?php echo $this->item->name; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_ALIAS'); ?></th>
			<td><?php echo $this->item->alias; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_CATID'); ?></th>
			<td><?php echo $this->item->catid_title; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_FEATURED'); ?></th>
			<td><?php echo $this->item->featured; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_IMAGE'); ?></th>
			<td><?php if ($this->item->image) { ?><img src="<?php echo $this->item->image; ?>" /><?php } ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_LANGUAGE'); ?></th>
			<td><?php echo $this->item->language; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_PLATFORM'); ?></th>
			<td><?php echo $this->item->platform; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_CHATBOX'); ?></th>
			<td><?php echo $this->item->chatbox; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_DESCRIPTION'); ?></th>
			<td><?php echo $this->item->description; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_STATE'); ?></th>
			<td><i class="icon-<?php echo ($this->item->state == 1) ? 'publish' : 'unpublish'; ?>"></i></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_CREATED_BY'); ?></th>
			<td><?php echo $this->item->created_by_name; ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_PARAMS_WEBSITE'); ?></th>
			<td><?php echo $this->item->params->get('website'); ?></td>
		</tr>
		<tr>
			<th><?php echo JText::_('COM_DZSTREAM_FORM_LBL_CHANNEL_PARAMS_FACEBOOK'); ?></th>
			<td><?php echo $this->item->params->get('facebook'); ?></td>
		</tr>
		<tr>
			<th>Embed</th>
			<td>
				<?php echo $this->item->channel->getEmbedCode(array(
				'width' => '100%',
				'height' => '320px',
				'style' => 'border: none;',
				'class' => 'stream-frame')); ?>
			</td>
		</tr>
		<?php if ($this->item->chatbox) : ?>
		<tr>
			<th>Chatbox</th>
			<td>
				<?php if ($this->item->channel->getChatboxCode()) : ; ?>
					<?php echo $this->item->channel->getChatboxCode(array(
						'width' => '100%',
						'height' => '500px',
						'style' => 'border: none;',
						'class' => 'chat-frame')); ?>
				<?php endif; ?>
			</td>
		</tr>
		<?php endif; ?>
	</table>
<?php
else:
	echo JText::_('COM_DZSTREAM_ITEM_NOT_LOADED');
endif;
?>
</div>
