<?php
/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('jquery.framework');
JFactory::getDocument()->addScript(JUri::root() . 'components/com_dzstream/assets/js/channel.js');
?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<div class="component-inner">
    <h1 class="heading"><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
    <table class="table table-hover table-striped" data-stream-live>
        <?php foreach ($this->items as $item) : ?>
        <tr data-stream-id="<?= $item->name; ?>" data-stream-platform="<?= $item->platform; ?>">
            <td width="40"><img src="<?= JUri::root() . 'components/com_dzstream/assets/img/' . $item->platform. '.ico'; ?>" alt="<?php echo $item->platform;?>" style="height:20px !important;"/></td>
            <td width="40"><small><?= $this->escape($item->language); ?></small></td>
            <td><a href="<?= $item->link; ?>" target="_blank" title="<?= $this->escape($item->title); ?>"><strong><?= $this->escape($item->title); ?></strong></a></td>
          
            <td width="80"><i class="fa fa-user"></i> <small data-stream-attrib="view_count" data-stream-attrib-transform='{"false": " ", "null": " "}'><i class="fa fa-spinner fa-spin"></i></small></td>
            <td width="60" align="right"><small data-stream-attrib="is_live" data-stream-attrib-transform='{"true": "LIVE", "false": "OFFLINE", "null": "-"}'><i class="fa fa-spinner fa-spin"></i></small></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php echo $this->pagination->getListFooter(); ?>
</div>
