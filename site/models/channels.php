<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
require_once JPATH_SITE.'/components/com_dzstream/helpers/dzstream.php';
require_once JPATH_SITE.'/components/com_dzstream/helpers/route.php';
require_once JPATH_ADMINISTRATOR . '/components/com_dzstream/helpers/channel.php';

/**
 * Methods supporting a list of Dzstream records.
 */
class DzstreamModelChannels extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param    array    An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				                'id', 'a.id',
                'ordering', 'a.ordering',
                'name', 'a.name',
                'alias', 'a.alias',
                'catid', 'a.catid',
                'featured', 'a.featured',
                'image', 'a.image',
                'language', 'a.language',
                'platform', 'a.platform',
                'chatbox', 'a.chatbox',
                'description', 'a.description',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'params', 'a.params',

			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{


		// Initialise variables.
		$app = JFactory::getApplication();

		// List state information
		$limitstart = $app->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);

		$catid = $app->input->get('catid', 0, 'int');
        $this->setState('filter.catid', $catid);

		$filter_language = $app->input->get('filter_language', '', 'cmd');
		if ($filter_language) {
			$this->setState('filter.language', $filter_language);
		}

		$filter_platform = $app->input->get('filter_platform', '', 'cmd');
		if ($filter_platform) {
			$this->setState('filter.platform', $filter_platform);
		}

		$input = JFactory::getApplication()->input;
        $menuitemid = $input->getInt( 'Itemid' );  // this returns the menu id number so you can reference parameters
        $menu = JSite::getMenu();
        if ($menuitemid) {
           $menuparams = $menu->getParams( $menuitemid );
           $this->setState('menuparams',$menuparams);
        }

		$this->setState('list.featured',$menuparams->get('featured', 1));
		$this->setState('list.limit', $menuparams->get('display_num', 10));

		$this->setState('list.filter_date',$menuparams->get('filter_date'));

        $this->setState('list.ordering',$menuparams->get('ordering', 'ordering'));
        $this->setState('list.direction',$menuparams->get('direction', 'asc'));
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return    JDatabaseQuery
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query
			->select(
				$this->getState(
					'list.select', 'DISTINCT a.*'
				)
			);

		$query->from('`#__dzstream_channels` AS a');


	    // Join over the users for the checked out user.
	    $query->select('uc.name AS editor');
	    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		// Join over the category 'catid'
		$query->select('catid.title AS catid_title');
		$query->join('LEFT', '#__categories AS catid ON catid.id = a.catid');
		// Join over the created by field 'created_by'
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');


		// Filter published
		$query->where('a.state = 1');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.name LIKE '.$search.' )');
			}
		}



		//Filtering catid
		$filter_catid = $this->state->get("filter.catid");
		if ($filter_catid) {
			$query->where("a.catid = '".$db->escape($filter_catid)."'");
		}

		//Filtering language
		$filter_language = $this->state->get("filter.language");
		if ($filter_language) {
			$query->where("a.language = '".$db->escape($filter_language)."'");
		}

		//Filtering platform
		$filter_platform = $this->state->get("filter.platform");
		if ($filter_platform) {
			$query->where("a.platform = '".$db->escape($filter_platform)."'");
		}

		// Define null and now dates
		$nullDate	= $db->quote($db->getNullDate());
		$nowDate	= $db->quote(JFactory::getDate()->toSql());

		$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
			->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');

		$filter_featured = $this->state->get('list.featured');
		if ($filter_featured == 0)
			$query->where("a.featured != 1");
		if ($filter_featured == 2)
			$query->order("featured DESC");

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	public function getItems()
	{
		$items = parent::getItems();
		foreach($items as $item){
			if ( isset($item->catid) ) {
			// Get the title of that particular template
				$title = DzstreamFrontendHelper::getCategoryNameByCategoryId($item->catid);

				// Finally replace the data object with proper information
				$item->catid = !empty($title) ? $title : $item->catid;
			}

            $item->link     = Jroute::_(DZStreamHelperRoute::getChannelRoute(implode(array($item->id,':',$item->alias)), $item->catid));
			$item->channel = new DZChannelHelper($item->platform, $item->name);
		}

		return $items;
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;
		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && !$this->isValidDate($value))
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}
		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_DZSTREAM_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in an specified format (YYYY-MM-DD)
	 *
	 * @param string Contains the date to be checked
	 *
	 */
	private function isValidDate($date)
	{
		return preg_match("/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/", $date) && date_create($date);
	}

}
