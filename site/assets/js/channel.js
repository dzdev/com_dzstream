jQuery(document).ready(function() {
    var $stream = jQuery('[data-stream-live]'), root;
    if ($stream.length > 0) {
        root = $stream.data('data-stream-root');
        if (root === undefined || root === null) {
            root = '/';
        }
        jQuery('[data-stream-id]', $stream).each(function() {
            var $this = jQuery(this),
                platform = $this.data('stream-platform'),
                id = $this.data('stream-id');
            jQuery.ajax({
                dataType: 'json',
                url: root + 'index.php',
                data: {
                    'option': 'com_dzstream',
                    'task': 'channel.liveAttribs',
                    'platform': platform,
                    'channel_id': id
                },
                success: function(result) {
                    jQuery('[data-stream-attrib]', $this).each(function() {
                        jQuery(this).removeClass('loading');
                        var attrib_value = null,
                            attrib_transform = jQuery(this).data('stream-attrib-transform'),
                            attrib_string;

                        if (result.data) {
                            attrib_value = result.data[jQuery(this).data('stream-attrib')];
                        }
                        attrib_string = String(attrib_value);

                        if (attrib_transform && attrib_transform[attrib_string]) {
                            jQuery(this).text(attrib_transform[attrib_string]);
                            jQuery(this).addClass(attrib_transform[attrib_string]);
                        } else {
                            if (attrib_value !== null)  {
                                jQuery(this).text(attrib_string);
                                jQuery(this).addClass(attrib_string);
                            }
                        }
                    });
                }
            });
        });
    }
});
