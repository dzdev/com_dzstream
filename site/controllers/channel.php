<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Channel controller class.
 */
class DzstreamControllerChannel extends DzstreamController {

    /**
     * Method to check out an item for editing and redirect to the edit form.
     *
     * @since	1.6
     */
    public function edit() {
        $app = JFactory::getApplication();

        // Get the previous edit id (if any) and the current edit id.
        $previousId = (int) $app->getUserState('com_dzstream.edit.channel.id');
        $editId = JFactory::getApplication()->input->getInt('id', null, 'array');

        // Set the user id for the user to edit in the session.
        $app->setUserState('com_dzstream.edit.channel.id', $editId);

        // Get the model.
        $model = $this->getModel('Channel', 'DzstreamModel');

        // Check out the item
        if ($editId) {
            $model->checkout($editId);
        }

        // Check in the previous user.
        if ($previousId && $previousId !== $editId) {
            $model->checkin($previousId);
        }

        // Redirect to the edit screen.
        $this->setRedirect(JRoute::_('index.php?option=com_dzstream&view=channelform&layout=edit', false));
    }

    /**
     * Method to save a user's profile data.
     *
     * @return	void
     * @since	1.6
     */
    public function publish() {
        // Initialise variables.
        $app = JFactory::getApplication();

        //Checking if the user can remove object
        $user = JFactory::getUser();
        if ($user->authorise('core.edit', 'com_dzstream') || $user->authorise('core.edit.state', 'com_dzstream')) {
            $model = $this->getModel('Channel', 'DzstreamModel');

            // Get the user data.
            $id = $app->input->getInt('id');
            $state = $app->input->getInt('state');

            // Attempt to save the data.
            $return = $model->publish($id, $state);

            // Check for errors.
            if ($return === false) {
                $this->setMessage(JText::sprintf('Save failed: %s', $model->getError()), 'warning');
            }

            // Clear the profile id from the session.
            $app->setUserState('com_dzstream.edit.channel.id', null);

            // Flush the data from the session.
            $app->setUserState('com_dzstream.edit.channel.data', null);

            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_DZSTREAM_ITEM_SAVED_SUCCESSFULLY'));
            $menu = & JSite::getMenu();
            $item = $menu->getActive();
            $this->setRedirect(JRoute::_($item->link, false));
        } else {
            throw new Exception(500);
        }
    }

    public function remove() {

        // Initialise variables.
        $app = JFactory::getApplication();

        //Checking if the user can remove object
        $user = JFactory::getUser();
        if ($user->authorise($user->authorise('core.delete', 'com_dzstream'))) {
            $model = $this->getModel('Channel', 'DzstreamModel');

            // Get the user data.
            $id = $app->input->getInt('id', 0);

            // Attempt to save the data.
            $return = $model->delete($id);


            // Check for errors.
            if ($return === false) {
                $this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
            } else {
                // Check in the profile.
                if ($return) {
                    $model->checkin($return);
                }

                // Clear the profile id from the session.
                $app->setUserState('com_dzstream.edit.channel.id', null);

                // Flush the data from the session.
                $app->setUserState('com_dzstream.edit.channel.data', null);

                $this->setMessage(JText::_('COM_DZSTREAM_ITEM_DELETED_SUCCESSFULLY'));
            }

            // Redirect to the list screen.
            $menu = & JSite::getMenu();
            $item = $menu->getActive();
            $this->setRedirect(JRoute::_($item->link, false));
        } else {
            throw new Exception(500);
        }
    }

    public function liveAttribs() {
        $app = JFactory::getApplication();

        $platform = $app->input->get('platform', NULL, 'cmd');
        $id = $app->input->get('channel_id', NULL, 'cmd' );
        JFactory::getDocument()->setMimeEncoding('application/json');
        $params= $app->getParams('com_dzstream');

        try {
            $cache = JFactory::getCache('com_dzstream_api', 'output');
            $cache->setCaching($params->get('api_caching', 1, 'int'));
            $cache->setLifetime($params->get('api_cache_lifetime', 5, 'int'));
            $data = $cache->get("{$platform}_{$id}");

            if ($data === FALSE) {
                $db = JFactory::getDbo();
                $db->setQuery("SELECT live, view FROM #__dzstream_channels WHERE platform = " . $db->quote($platform) . " AND name = " . $db->quote($id));
                $result = $db->loadObject();
                if (!$result) {
                    $cache->store(NULL, "{$platform}_{$id}");
                    throw new Exception("Channel not found");
                }
                $data = json_encode(array(
                    'data' => array(
                        'platform' => $platform,
                        'id' => $id,
                        'is_live' => ($result->live === NULL) ? NULL : (bool) $result->live,
                        'view_count' => $result->view
                    )
                ));
                $cache->store($data, "{$platform}_{$id}");
            } else {
                if ($data === NULL) {
                    throw new Exception("Channel not found");
                }
            }

            echo $data;
        } catch (Exception $e) {
            echo json_encode(array('errors' => array($e->getMessage())));
        }
        $app->close();
    }
}
