<?php
/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

include_once JPATH_COMPONENT_ADMINISTRATOR . '/helpers/channel.php';
/**
 * Channel controller class.
 */
class DzstreamControllerChannel extends JControllerForm
{

    function __construct() {
        $this->view_list = 'channels';
        parent::__construct();
    }

    function check() {
        JFactory::getDocument()->setMimeEncoding('application/json');

        $input = JFactory::getApplication()->input;
        $id = $input->get('id', null, 'cmd');
        $platform = $input->get('platform', null, 'cmd');

        try {
            $helper = new DZChannelHelper($platform, $id);
            echo json_encode(array('valid' => $helper->isValid()));
        } catch (Exception $e) {
            echo json_encode(array('valid' => false, 'message' => $e->getMessage()));
        }

        JFactory::getApplication()->close();
    }
    
    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        $recordId = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $userId = $user->get('id');

        // Check general edit permission first.
        if ($user->authorise('core.edit', 'com_dzstream.channel.' . $recordId))
        {
            return true;
        }

        // Fallback on edit.own.
        // First test if the permission is available.
        if ($user->authorise('core.edit.own', 'com_dzstream.channel.' . $recordId))
        {
            // Now test the owner is the user.
            $ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId)
            {
                // Need to do a lookup from the model.
                $record = $this->getModel()->getItem($recordId);

                if (empty($record))
                {
                    return false;
                }

                $ownerId = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($ownerId == $userId)
            {
                return true;
            }
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }
}
