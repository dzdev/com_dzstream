<?php
/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dzstream/assets/css/dzstream.css');

$user    = JFactory::getUser();
$userId    = $user->get('id');
$listOrder    = $this->state->get('list.ordering');
$listDirn    = $this->state->get('list.direction');
$canOrder    = $user->authorise('core.edit.state', 'com_dzstream');
$saveOrder    = $listOrder == 'a.ordering';
if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_dzstream&task=channels.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'channelList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
    Joomla.orderTable = function() {
        table = document.getElementById("sortTable");
        direction = document.getElementById("directionTable");
        order = table.options[table.selectedIndex].value;
        if (order != '<?= $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }

    jQuery(document).ready(function () {
        jQuery('#clear-search-button').on('click', function () {
            jQuery('#filter_search').val('');
            jQuery('#adminForm').submit();
        });
    });
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?= JRoute::_('index.php?option=com_dzstream&view=channels'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?= $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif;?>

        <div id="filter-bar" class="btn-toolbar">
            <div class="filter-search btn-group pull-left">
                <label for="filter_search" class="element-invisible"><?= JText::_('JSEARCH_FILTER');?></label>
                <input type="text" name="filter_search" id="filter_search" placeholder="<?= JText::_('JSEARCH_FILTER'); ?>" value="<?= $this->escape($this->state->get('filter.search')); ?>" title="<?= JText::_('JSEARCH_FILTER'); ?>" />
            </div>
            <div class="btn-group pull-left">
                <button class="btn hasTooltip" type="submit" title="<?= JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
                <button class="btn hasTooltip" id="clear-search-button" type="button" title="<?= JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
            </div>
            <div class="btn-group pull-right hidden-phone">
                <label for="limit" class="element-invisible"><?= JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
                <?= $this->pagination->getLimitBox(); ?>
            </div>
            <div class="btn-group pull-right hidden-phone">
                <label for="directionTable" class="element-invisible"><?= JText::_('JFIELD_ORDERING_DESC');?></label>
                <select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
                    <option value=""><?= JText::_('JFIELD_ORDERING_DESC');?></option>
                    <option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?= JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
                    <option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?= JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
                </select>
            </div>
            <div class="btn-group pull-right">
                <label for="sortTable" class="element-invisible"><?= JText::_('JGLOBAL_SORT_BY');?></label>
                <select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
                    <option value=""><?= JText::_('JGLOBAL_SORT_BY');?></option>
                    <?= JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
                </select>
            </div>
        </div>
        <div class="clearfix"> </div>
        <table class="table table-striped" id="channelList">
            <thead>
                <tr>
                <?php if (isset($this->items[0]->ordering)): ?>
                    <th width="1%" class="nowrap center hidden-phone">
                        <?= JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
                    </th>
                <?php endif; ?>
                    <th width="1%" class="hidden-phone">
                        <input type="checkbox" name="checkall-toggle" value="" title="<?= JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                    </th>
                <?php if (isset($this->items[0]->state)): ?>
                    <th width="1%" class="nowrap center">
                        <?= JHtml::_('grid.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>
                <th class='left' width="10%">
                <?= JText::_('COM_DZSTREAM_CHANNELS_IMAGE'); ?>
                </th>
                <th class='left'>
                <?= JHtml::_('grid.sort',  'COM_DZSTREAM_CHANNELS_TITLE', 'a.title', $listDirn, $listOrder); ?>
                </th>
                <th class='left'>
                <?= JHtml::_('grid.sort',  'COM_DZSTREAM_CHANNELS_LANGUAGE', 'a.language', $listDirn, $listOrder); ?>
                </th>
                <th class='left'>
                <?= JHtml::_('grid.sort',  'COM_DZSTREAM_CHANNELS_PLATFORM', 'a.platform', $listDirn, $listOrder); ?>
                </th>
                <th class="center">
                <?= JHtml::_('grid.sort', 'COM_DZSTREAM_CHANNELS_LIVE', 'a.live', $listDirn, $listOrder); ?>
                </th>
                <th class="center">
                <?= JHtml::_('grid.sort', 'COM_DZSTREAM_CHANNELS_VIEW', 'a.view', $listDirn, $listOrder); ?>
                </th>
                <th>
                <?= JHtml::_('grid.sort', 'COM_DZSTREAM_CHANNELS_MODIFIED', 'a.modified', $listDirn, $listOrder); ?>
                </th>
                <th class='left'>
                <?= JHtml::_('grid.sort',  'COM_DZSTREAM_CHANNELS_CREATED_BY', 'created_by_name', $listDirn, $listOrder); ?>
                </th>


                <?php if (isset($this->items[0]->id)): ?>
                    <th width="1%" class="nowrap center hidden-phone">
                        <?= JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>
                </tr>
            </thead>
            <tfoot>
                <?php
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
            <tr>
                <td colspan="<?= $colspan ?>">
                    <?= $this->pagination->getListFooter(); ?>
                </td>
            </tr>
            </tfoot>
            <tbody>
            <?php foreach ($this->items as $i => $item) :
                $ordering   = ($listOrder == 'a.ordering');
                $canCreate    = $user->authorise('core.create',        'com_dzstream');
                $canEdit    = $user->authorise('core.edit',            'com_dzstream.channel.' . $item->id);
                $canEditOwn = $user->authorise('core.edit.own',        'com_dzstream.channel.' . $item->id) && $item->created_by == $userId;
                $canCheckin    = $user->authorise('core.manage',        'com_dzstream');
                $canChange    = $user->authorise('core.edit.state',    'com_dzstream.channel.' . $item->id);
                ?>
                <tr class="row<?= $i % 2; ?>">

                <?php if (isset($this->items[0]->ordering)): ?>
                    <td class="order nowrap center hidden-phone">
                    <?php if ($canChange) :
                        $disableClassName = '';
                        $disabledLabel      = '';
                        if (!$saveOrder) :
                            $disabledLabel    = JText::_('JORDERINGDISABLED');
                            $disableClassName = 'inactive tip-top';
                        endif; ?>
                        <span class="sortable-handler hasTooltip <?= $disableClassName?>" title="<?= $disabledLabel?>">
                            <i class="icon-menu"></i>
                        </span>
                        <input type="text" style="display:none" name="order[]" size="5" value="<?= $item->ordering;?>" class="width-20 text-area-order " />
                    <?php else : ?>
                        <span class="sortable-handler inactive" >
                            <i class="icon-menu"></i>
                        </span>
                    <?php endif; ?>
                    </td>
                <?php endif; ?>
                    <td class="hidden-phone">
                        <?= JHtml::_('grid.id', $i, $item->id); ?>
                    </td>
                <?php if (isset($this->items[0]->state)): ?>
                    <td class="center">
                        <div class="btn-group">
                            <?= JHtml::_('jgrid.published', $item->state, $i, 'channels.', $canChange, 'cb'); ?>
                            <?= JHtml::_('channeladministrator.featured', $item->featured, $i, $canChange); ?>
                        </div>
                    </td>
                <?php endif; ?>
                <td>
                    <?php if ($item->image) { ?><img src="<?= JUri::root() . $item->image; ?>" /><?php } ?>
                </td>
                <td>
                <?php if (isset($item->checked_out) && $item->checked_out) : ?>
                    <?= JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'channels.', $canCheckin); ?>
                <?php endif; ?>
                <?php if ($canEdit || $canEditOwn) : ?>
                    <a href="<?= JRoute::_('index.php?option=com_dzstream&task=channel.edit&id='.(int) $item->id); ?>">
                    <?= $this->escape($item->title); ?></a>
                <?php else : ?>
                    <?= $this->escape($item->title); ?>
                <?php endif; ?>
                    <?php if ($item->channel_title) { ?>
                    <div class="small"><?= JText::_('COM_DZSTREAM_CHANNELS_CHANNEL_TITLE') ?>: <?= $item->channel_title ?></div>
                    <?php } ?>
                    <?php if ($item->catid) { ?>
                    <div class="small"><?= JText::_('COM_DZSTREAM_CHANNELS_CATID')?>: <?= $item->catid; ?></div>
                    <?php } ?>
                </td>
                <td>
                    <?= $item->language; ?>
                </td>
                <td>
                    <?= $item->platform; ?>
                </td>
                <td class="center">
                    <?php if ($item->live === NULL) : ?>
                        -
                    <?php else : if ($item->live) : ?>
                        <span class="label label-success"><?= JText::_('JYES'); ?></span>
                    <?php else : ?>
                        <span class="label label-default"><?= JText::_('JNO'); ?></span>
                    <?php endif; endif; ?>
                </td>
                <td class="center">
                    <?= ($item->view === NULL) ? '-' : $item->view; ?>
                </td>
                <td>
                    <?= $item->modified; ?>
                </td>
                <td>
                    <?= $item->created_by_name; ?>
                </td>


                <?php if (isset($this->items[0]->id)): ?>
                    <td class="center hidden-phone">
                        <?= (int) $item->id; ?>
                    </td>
                <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?= $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?= $listDirn; ?>" />
        <?= JHtml::_('form.token'); ?>
    </div>
</form>
