<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Dzstream.
 */
class DzstreamViewChannels extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        DzstreamHelper::addSubmenu('channels');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/dzstream.php';

        $state = $this->get('State');
        $canDo = DzstreamHelper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_DZSTREAM_TITLE_CHANNELS'), 'channels.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/channel';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolBarHelper::addNew('channel.add', 'JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('channel.edit', 'JTOOLBAR_EDIT');
            }
        }

        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('channels.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('channels.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'channels.delete', 'JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('channels.archive', 'JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('channels.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }

        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'channels.delete', 'JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('channels.trash', 'JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_dzstream');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_dzstream&view=channels');

        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(
			JText::_("JOPTION_SELECT_CATEGORY"),
			'filter_catid',
			JHtml::_('select.options', JHtml::_('category.options', 'com_dzstream'), "value", "text", $this->state->get('filter.catid'))

		);

		//Filter for the field language
		$select_label = JText::sprintf('COM_DZSTREAM_FILTER_SELECT_LABEL', 'Language');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "en";
		$options[0]->text = "English";
		$options[1] = new stdClass();
		$options[1]->value = "vi";
		$options[1]->text = "Vietnamese";
		$options[2] = new stdClass();
		$options[2]->value = "cn";
		$options[2]->text = "Chinese";
		$options[3] = new stdClass();
		$options[3]->value = "kr";
		$options[3]->text = "Korean";
		$options[4] = new stdClass();
		$options[4]->value = "ru";
		$options[4]->text = "Russian";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_language',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.language'), true)
		);

		//Filter for the field platform
		$select_label = JText::sprintf('COM_DZSTREAM_FILTER_SELECT_LABEL', 'Platform');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "talktv";
		$options[0]->text = "TalkTV";
		$options[1] = new stdClass();
		$options[1]->value = "twitch";
		$options[1]->text = "Twitch";
		$options[2] = new stdClass();
		$options[2]->value = "hitbox";
		$options[2]->text = "Hitbox";
		$options[3] = new stdClass();
		$options[3]->value = "azubu";
		$options[3]->text = "Azubu";
		$options[4] = new stdClass();
		$options[4]->value = "huomao";
		$options[4]->text = "HuoMao.TV";
		$options[5] = new stdClass();
		$options[5]->value = "douyu";
		$options[5]->text = "Douyu";
		$options[6] = new stdClass();
		$options[6]->value = "zhanqi";
		$options[6]->text = "ZhanqiTV";
        $options[7] = new stdClass();
        $options[7]->value = "youtube";
        $options[7]->text = "Youtube";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_platform',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.platform'), true)
		);

		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);

    }

	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.name' => JText::_('COM_DZSTREAM_CHANNELS_NAME'),
		'a.catid' => JText::_('COM_DZSTREAM_CHANNELS_CATID'),
		'a.featured' => JText::_('COM_DZSTREAM_CHANNELS_FEATURED'),
		'a.image' => JText::_('COM_DZSTREAM_CHANNELS_IMAGE'),
		'a.language' => JText::_('JGRID_HEADING_LANGUAGE'),
		'a.platform' => JText::_('COM_DZSTREAM_CHANNELS_PLATFORM'),
		'a.state' => JText::_('JSTATUS'),
		);
	}

}
