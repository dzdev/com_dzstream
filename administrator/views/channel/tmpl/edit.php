<?php
/**
* @version     1.0.0
* @package     com_dzstream
* @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
* @license     bản quyền mã nguồn mở GNU phiên bản 2
* @author      DZ Team <support@dzdev.com> - dzdev.com
*/
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dzstream/assets/css/dzstream.css');
$document->addScript('components/com_dzstream/assets/javascript/channel.js');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {

    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'channel.cancel') {
            Joomla.submitform(task, document.getElementById('channel-form'));
        }
        else {

            if (task != 'channel.cancel' && document.formvalidator.isValid(document.id('channel-form'))) {

                Joomla.submitform(task, document.getElementById('channel-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dzstream&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="channel-form" class="form-validate">
    <?php echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DZSTREAM_TITLE_CHANNEL', true)); ?>
        <div class="row-fluid">
            <div class="span9 form-horizontal">
                <fieldset class="adminform">
                <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
                            <div class="controls">
                                <?php echo $this->form->getInput('name'); ?>
                                <a href="#" id="check-channel" class="btn hasTooltip" title="Check"><i class="icon-loop" style="margin-right: 0px;"></i></a>
                                <span id="check-message"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('platform'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('platform'); ?></div>
                        </div>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('chatbox'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('chatbox'); ?></div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('image'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('image'); ?></div>
                        </div>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('website', 'params'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('website', 'params'); ?></div>
                        </div>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('facebook', 'params'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('facebook', 'params'); ?></div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                </div>
                <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
                <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
                </fieldset>
            </div>
            <div class="span3">
                <!-- Begin Sidebar -->
                <?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
                <!-- End Sidebar -->
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DZSTREAM_PUBLISHING', true)); ?>
            <div class="row-fluid">
                <div class="span6">
                     <?php echo JLayoutHelper::render('joomla.edit.publishingdata', $this); ?>
                </div>
                <div class="span6">
                    <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php if (JFactory::getUser()->authorise('core.admin','dzstream')) : ?>
    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
        <?php echo $this->form->getInput('rules'); ?>
    <?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
