ALTER TABLE `#__dzstream_channels`
ADD `live` TINYINT(1) NULL AFTER `description`,
ADD `view` INT(11) NULL AFTER `live`;
