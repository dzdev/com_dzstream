jQuery(document).ready(function() {
    jQuery("#jform_name").keyup(function(event){
        if(event.keyCode == 13){
            jQuery("#check-channel").click();
        }
    });
    jQuery('#check-channel').on('click', function(e) {
        e.preventDefault();
        var id = jQuery('#jform_name').val(),
            platform = jQuery('#jform_platform').val();

        jQuery('#check-channel').removeClass('btn-success btn-danger').addClass('btn-loading');
        jQuery('#check-message').text('');
        jQuery.getJSON('index.php?option=com_dzstream&task=channel.check&platform=' + platform + '&id=' + id, function(data) {
            if (data.valid) {
                jQuery('#check-channel').addClass('btn-success').removeClass('btn-danger btn-loading');
                jQuery('#check-message').text('Success');
            } else {
                jQuery('#check-channel').addClass('btn-danger').removeClass('btn-success btn-loading');
                if (data.message) {
                    jQuery('#check-message').text(data.message);
                } else {
                    jQuery('#check-message').text('Failed');
                }
            }
        });
        return false;
    });
});
