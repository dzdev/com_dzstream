<?php
/**
 * @package    DZStream.Daemon
 *
 * @copyright  Copyright (C) 2015 DZ Team
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * This is a CRON script which should be called from the command-line, not the
 * web. For example something like:
 * /usr/bin/php /path/to/component/daemon/dzstream.php
 */

// Set flag that this is a parent file.
const _JEXEC = 1;

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);
define('JPATH_BASE', dirname(__DIR__) . '/../../../');

// Load system defines
if (file_exists(JPATH_BASE . '/defines.php'))
{
	require_once JPATH_BASE . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';
require_once dirname(__DIR__) . '/helpers/channel.php';

class StreamUpdater extends JApplicationDaemon
{
    protected $name;
    
    public function __construct(JInputCli $input = null, Registry $config = null, JEventDispatcher $dispatcher = null)
	{
        $input = new JInputCli;
        $identifier = $input->get('id');
        
        $config = new JRegistry;
        $config->set('author_name', 'DZ Team');
        $config->set('author_email', 'dev@dzdev.com');
        $config->set('application_name', "Stream Updater Daemon" . ( ($identifier) ? " ($identifier)" : ''));
        $config->set('application_description', 'Run on background to update channel live status and view count');
        
        // Call the parent constructor.
        parent::__construct($input, $config, $dispatcher);
        
        $this->name = "Stream Updater Daemon" . ($identifier ? " ($identifier)" : '');
    }
    
    /**
     *
     */
    public function doExecute()
    {
        usleep(1000000);
        // Get list of all ids for current run
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id, name, platform FROM #__dzstream_channels");
        $channels = $db->loadObjectList();
        $model = JModelLegacy::getInstance('Channel', 'DZStreamModel');
        foreach ($channels as $channel) {
            try {
                $channelHelper = new DZChannelHelper($channel->platform, $channel->name);
                JLog::add("Getting info for $channel->platform $channel->name...");
                $live = $channelHelper->isLive();
                $view = $channelHelper->getViewCount();
				$title = $channelHelper->getTitle();
                
                $query = $db->getQuery(true);
                $query->update('#__dzstream_channels');
                $query->set('live = ' . ($live === NULL ? 'NULL' : (int) $live));
                $query->set('view = ' . ($view === NULL ? 'NULL' : (int) $view));
				$query->set('channel_title = ' . $db->quote((string) $title));
                $query->set('modified = ' . $db->quote((new JDate())->toSql()));
                $query->set('modified_by = 0'); // Indicate bot
                $query->where('id = ' . $channel->id);
                $db->setQuery($query);
                $db->execute();
            } catch (Exception $e){
                JLog::add($e->getMessage(), JLog::NOTICE);
            }
        };
    }
}
// Put log to stdout
JLog::addLogger(array('logger' => 'echo'));
JApplicationDaemon::getInstance('StreamUpdater')->execute();
