<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelDouyuHelper implements channelHelper {
    private $_id;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://www.douyutv.com/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->head($this->getLink());

        return ($response->code == 200);
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<embed
            src='http://staticlive.douyutv.com/common/share/play.swf?room_id={$this->_id}'
            allownetworking='all' allowscriptaccess='always' quality='high' bgcolor='#000' wmode='transparent'
            allowfullscreen='true' allowFullScreenInteractive='true' type='application/x-shockwave-flash'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}'></embed>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        // This platform doesn't support chatbox embed yet
        return false;
    }

    /** Not supported */
    public function isLive() {
        return NULL;
    }

    /** Not supported */
    public function getViewCount() {
        return NULL;
    }
    
    /** Not supported */
    public function getTitle() {
        return NULL;
    }
}
