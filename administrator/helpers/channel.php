<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

interface channelHelper {

    /**
     * Check if the channel is valid
     * @return boolean
     */
    public function isValid();

    /**
     * Return the link of the channel
     * @return string
     */
    public function getLink();

    /**
     * Get the video embed code
     * @param array $options specify width, height, class, style
     * @return string
     */
    public function getEmbedCode($options = array());

    /**
     * Get the chatbox code
     * @param array $options specify width, height, class, style
     * @return string
     */
     public function getChatboxCode($options = array());

    /**
     * Check whether this channel is live
     * @return boolean|NULL
     */
    public function isLive();

    /**
     * Check view count
     * @return integer|NULL
     */
    public function getViewCount();
    
    /**
     * Get the title of the channel
     * @return string|NULL
     */
    public function getTitle();
}

/**
 * Channel helper.
 */
class DZChannelHelper {
    private $_channel;
    private $_platform;
    private $_id;

    /* Initialize the delegator */
    public function __construct($platform, $id) {
        if (!JFile::exists(dirname(__FILE__) . '/channel.' . $platform . '.php' )) {
            throw new Exception("Platform $platform is not supported!");
        } else {
            if ($id == NULL) {
                throw new Exception("Channel ID not specified");
            }
            include_once dirname(__FILE__) . '/channel.' . $platform . '.php';
            $class = "DZChannel" . $platform . "Helper";
            if (!class_exists($class)) {
                throw new Exception("Helper for $platform not found");
            }
            $this->_channel = new $class($id);
            $this->_platform = $platform;
            $this->_id = $id;
        }
    }

    /**
     * Return the channel id
     * @return string
     */
    public function id() {
        return $this->_id;
    }

    /**
     * Return the channel platform
     * @return string
     */
    public function platform() {
        return $this->_platform;
    }

    /** Proxy call to delegation */
    public function __call($name, $arguments) {
        return call_user_func_array(array($this->_channel, $name), $arguments);
    }
}
