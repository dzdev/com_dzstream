<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelYoutubeHelper implements channelHelper {
    private $_id;
    private $_api_key;
    private $_stream_api_response;

    public function __construct($id) {
        $this->_id = $id;
        $this->_api_key = JComponentHelper::getParams('com_dzstream')->get('youtube_api_key');
    }

    public function getLink() {
        return "https://www.youtube.com/watch?v={$this->_id}";
    }

    public function isValid() {
        $api = $this->_stream_api();

        return (!empty($api) && count($api['items']) == 1 && array_key_exists('liveStreamingDetails', $api['items'][0]));
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='https://www.youtube.com/embed/{$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        return "";
    }

    public function isLive() {
        if ($this->isValid()) {
            $api = $this->_stream_api();
            return !array_key_exists('actualEndTime', $api['items'][0]['liveStreamingDetails']);
        }

        return false;
    }

    public function getViewCount() {
        if ($this->isValid() && $this->isLive()) {
            $api = $this->_stream_api();
            return $api['items'][0]['liveStreamingDetails']['concurrentViewers'];
        }
        
        return 0;
    }
    
    public function getTitle() {
        if ($this->isValid()) {
            $api = $this->_stream_api();
            
            return $api['items'][0]['snippet']['title'];
        }
        
        return NULL;
    }

    private function _stream_api() {
        if ($this->_stream_api_response === NULL) {
            $this->_stream_api_response = JHttpFactory::getHttp()->get("https://www.googleapis.com/youtube/v3/videos?id={$this->_id}&part=liveStreamingDetails,snippet&key={$this->_api_key}", null, 30);
        }

        return json_decode($this->_stream_api_response->body, true);
    }
}
