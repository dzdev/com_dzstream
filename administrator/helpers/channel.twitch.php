<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelTwitchHelper implements channelHelper {
    private $_id;
    private $_stream_api_response;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://www.twitch.tv/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->get("https://api.twitch.tv/kraken/channels/{$this->_id}");
        $result = json_decode($response->body, true);

        return (!empty($result) && !isset($result['error']));
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.twitch.tv/{$this->_id}/embed'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.twitch.tv/{$this->_id}/chat?popout='
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function isLive() {
        $api = $this->_stream_api();

        if (isset($api['stream']) && !empty($api['stream']))
            return true;

        return false;
    }

    public function getViewCount() {
        $api = $this->_stream_api();

        if (isset($api['stream']) && !empty($api['stream']))
            return $api['stream']['viewers'];

        return 0;
    }
    
    public function getTitle() {
        $api = $this->_stream_api();
        
        if (isset($api['stream']) && !empty($api['stream']))
            return $api['stream']['channel']['status'];
        
        return NULL;
    }

    private function _stream_api() {
        if ($this->_stream_api_response === NULL) {
            $this->_stream_api_response = JHttpFactory::getHttp()->get("https://api.twitch.tv/kraken/streams/{$this->_id}", null, 30);
        }

        return json_decode($this->_stream_api_response->body, true);
    }
}
