<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelHuomaoHelper implements channelHelper {
    private $_id;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://www.huomaotv.com/live/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->get($this->getLink());

        return (strpos($response->body, "id=\"error\"") === false);
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.huomaotv.com/index.php?c=outplayer&live_id={$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        // This platform doesn't support chatbox embed
        return false;
    }

    /** Not supported */
    public function isLive() {
        return NULL;
    }

    public function getViewCount() {
        $response = JHttpFactory::getHttp()->get("http://tongji.huomaotv.com/index.php?c=ajax&a=get_live_views&cid={$this->_id}", null, 30); // JSONP
        $result = json_decode(substr($response->body, 1, -1), true);

        if ($result && $result['val'])
            return $result['val'];

        return NULL;
    }
    
    public function getTitle() {
        $response = JHttpFactory::getHttp()->get("http://www.huomaotv.com/live/{$this->_id}", null, 30);
        $matches = array();
        if (preg_match('/h1 title="(.*?)"/', $response->body, $matches)) {
            return $matches[1];
        }
        
        return NULL;
    }
}
