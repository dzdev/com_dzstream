<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelAzubuHelper implements channelHelper {
    private $_id;
    private $_api_response;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://www.azubu.tv/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->head($this->getLink());

        return ($response->code == 200);
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.azubu.tv/azubulink/embed={$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.azubu.tv/{$this->_id}/chatpopup?chatroom=azubu.{$this->_id}.en'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}'></iframe>";
    }

    public function isLive() {
        return $this->_get('is_live', false);
    }

    public function getViewCount() {
        return $this->_get('view_count');
    }
    
    public function getTitle() {
        return $this->_get('title');
    }

    private function _api() {
        if ($this->_api_response === NULL) {
            $this->_api_response = JHttpFactory::getHttp()->get("https://api.azubu.tv/public/channel/{$this->_id}", null, 30);
        }

        return json_decode($this->_api_response->body, true);
    }
    
    private function _get($field, $default = NULL) {
        $api = $this->_api();
        if (isset($api['data']) && isset($api['data'][$field]))
            return $api['data'][$field];
        
        return $default;
    }
}
