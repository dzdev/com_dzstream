<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Dzstream helper.
 */
class DZStreamHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
        		JHtmlSidebar::addEntry(
			JText::_('COM_DZSTREAM_TITLE_CHANNELS'),
			'index.php?option=com_dzstream&view=channels',
			$vName == 'channels'
		);
		JHtmlSidebar::addEntry(
			JText::_('JCATEGORIES') . ' (' . JText::_('COM_DZSTREAM_TITLE_CHANNELS') . ')',
			"index.php?option=com_categories&extension=com_dzstream",
			$vName == 'categories'
		);
		if ($vName=='categories') {
			JToolBarHelper::title('Stream Channels: JCATEGORIES (COM_DZSTREAM_TITLE_CHANNELS)');
		}

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_dzstream';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


}
