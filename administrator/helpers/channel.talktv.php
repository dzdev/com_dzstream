<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelTalkTVHelper implements channelHelper {
    private $_id;
    private $_api_response;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://talktv.vn/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->get($this->getLink());

        return (strpos($response->body, "error-404") === false);
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://talktv.vn/streaming/play/embed/{$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://talktv.vn/streaming/chat/embed/{$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function isLive() {
        $api = $this->_api();

        // Scraping the web, unreliable
        if (strpos($api, "loadPlayer.live = 0") !== FALSE) {
            return false;
        } elseif (strpos($api, "loadPlayer.live = 1") !== FALSE) {
            return true;
        }

        return NULL;
    }

    public function getViewCount() {
        $api = $this->_api();
        
        if ($api && preg_match("/player-viewing-count\">(.*?)<\/div>/", $api, $matches)) {
            return (int) str_replace(',', '', $matches[1]);
        }
        
        return NULL;
    }
    
    public function getTitle() {
        $api = $this->_api();
        
        if ($api && preg_match('/h1 id="broadcast-title".*?>(.*?)</', $api, $matches)) {
            return $matches[1];
        }
        
        return NULL;
    }

    private function _api() {
        if ($this->_api_response === NULL)
            $this->_api_response = JHttpFactory::getHttp()->get("http://talktv.vn/{$this->_id}", null, 30);

        return $this->_api_response->body;
    }
}
