<?php

/**
 * @version     1.0.0
 * @package     com_dzstream
 * @copyright   Bản quyền (C) 2015. Các quyền đều được bảo vệ.
 * @license     bản quyền mã nguồn mở GNU phiên bản 2
 * @author      DZ Team <support@dzdev.com> - dzdev.com
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Channel helper.
 */
class DZChannelHitboxHelper implements channelHelper {
    private $_id;
    private $_api_response;

    public function __construct($id) {
        $this->_id = $id;
    }

    public function getLink() {
        return "http://www.hitbox.tv/{$this->_id}";
    }

    public function isValid() {
        $http = JHttpFactory::getHttp();
        $response = $http->head($this->getLink());

        return ($response->code == 200);
    }

    public function getEmbedCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.hitbox.tv/embed/{$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function getChatboxCode($options = array()) {
        $default = array('width' => '100%', 'height' => '100%', 'style' => '', 'class' => '');
        $options = array_merge($default, $options);

        return "<iframe
            src='http://www.hitbox.tv/embedchat/{$this->_id}'
            width='{$options['width']}'
            height='{$options['height']}'
            style='{$options['style']}'
            class='{$options['class']}' allowfullscreen></iframe>";
    }

    public function isLive() {
        return (bool) $this->_get('media_is_live', false);
    }

    public function getViewCount() {
        return $this->_get('media_views', 0);
    }
    
    public function getTitle() {
        return $this->_get('media_status');
    }

    private function _api() {
        if ($this->_api_response === NULL) {
            $this->_api_response = JHttpFactory::getHttp()->get("https://api.hitbox.tv/media/live/{$this->_id}", null, 30);
        }

        return json_decode($this->_api_response->body, true);
    }
    
    private function _get($field, $default = NULL) {
        $api = $this->_api();
        
        if ($api != NULL && isset($api['livestream'][0][$field]))
            return $api['livestream'][0][$field];
            
        return $default;
    }
}
